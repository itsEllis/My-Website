import type { NextPage } from "next";
import LeftPanel from "../components/left-panel";
import RightPanel from "../components/right-panel";

import { FaPhoneVolume } from "react-icons/fa6";

const Home: NextPage = () => {
  return (
    <div className=" bg-black w-screen bg-[url('https://gitlab.com/uploads/-/system/user/avatar/11730342/avatar.png?width=64')] h-screen flex justify-center items-center">
      {/* <div className="flex justify-center pt-32"> */}
        {/* Left column */}
        <LeftPanel />

        {/* Right column */}
        {/* <RightPanel /> */}
      </div>
    // </div>
  );
};

export default Home;
